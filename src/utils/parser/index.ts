import { DepartmentList } from '../../store/state'

interface CodeName {
  code: string
  name: string
}

type Row = [CodeName, CodeName | null, CodeName | null]

export const parse = (data: string) =>
  data
  // remove inconsistencies between newlines on unix and windows text files
    .replace(/\r\n/g, '\n')
    // split by newlines
    .split('\n')
    // only include the lines that comply with the desired format (department is always required)
    .filter(row =>
      /^\s*\d{1,2}\s+[^\/]+(?:\/\s*(?:\d{1,3}\s+[^\/]+)?){2}\s*$/.test(row)
    )
    // split lines by separator '/' and try to obtain the respective code and name, otherwise return null
    .map(row =>
      row.split('/').map(column => {
        // extract code and name, spread name as it can contain white spaces sometimes e.g. "San Isidro"
        const [code, ...name] = column.trim().split(' ')
        // get full name e.g. ["San", "Isidro"] -> "San Isidro", [""] -> ""
        const fullName = name.join(' ')
        return !code && !fullName ? null : {code, name: fullName}
      })
    ) as Row[]

// organize rows inside an object and remove duplications
export const merge = (row: Row[]) =>
  row.reduce(
    (
      accumulator,
      [{code: departmentCode, name: departmentName}, province, district]
    ) => {
      // get reference to the object belonging to the department code, if does not exist, assign a predefined value
      const department = accumulator[departmentCode] || {
        name: departmentName,
        provinces: {}
      }
      // assign reference to value at departmentCode, so if it did not previously exist, it will now be available
      accumulator[departmentCode] = department
      if (province) {
        const {code: provinceCode, name: provinceName} = province
        const departmentProvince = department.provinces[provinceCode] || {
          name: provinceName,
          districts: {}
        }
        department.provinces[provinceCode] = departmentProvince
        if (district) {
          const {code: districtCode, name: districtName} = district
          departmentProvince.districts[districtCode] = departmentProvince.districts[
            districtCode
            ] || {name: districtName}
        }
      }
      return accumulator
    },
    {} as DepartmentList
  )