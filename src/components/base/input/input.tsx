import React from 'react'
import classes from './styles.module.scss'

interface InputProps extends React.InputHTMLAttributes<HTMLInputElement> {
  type?: string,
  onChange?: (e?: React.ChangeEvent<HTMLInputElement> | any) => void,
  className?: string,
  disabled?: boolean
}

const Input: React.FC<InputProps> = ({type, className, onChange, ...rest}) => (
  <input className={`${classes.Button} ${className}`} type={type} onChange={onChange} {...rest} />
)

export default Input