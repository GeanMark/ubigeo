import { connect } from 'react-redux'
import { DepartmentList } from '../../../store/state'
import HeaderWrapper from '../../../components/home/header-wrapper'
import { DepartmentsActionTypes } from '../../../store/types'

const mapDispatchToProps = (dispatch: any) => {
  return {
    populateDepartments: (departments: DepartmentList) => dispatch({
      type: DepartmentsActionTypes.POPULATE_DATA,
      departments,
    })
  }
}

export default connect(null, mapDispatchToProps)(HeaderWrapper)
