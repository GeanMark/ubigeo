import React from 'react'
import TableWrapper from '../../../containers/home/table-wrapper'
import HeaderWrapper from '../../../containers/home/header-wrapper'

const MainLayout: React.FC = () => (
  <div>
    <HeaderWrapper />
    <TableWrapper />
  </div>
)

export default MainLayout