import { connect } from 'react-redux'
import { ReduxState } from '../../../store/state'
import TableWrapper from '../../../components/home/table-wrapper'

const mapStateToProps = ({departments}: ReduxState) => ({
  departments,
})

export default connect(mapStateToProps)(TableWrapper)
