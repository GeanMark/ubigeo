import React from 'react'
import Table from '../../../components/home/table'
import classes from './styles.module.scss'
import { DepartmentList } from '../../../store/state'

// format object and pair by key and value.name
export const format = (data: DepartmentList) => {
  let departments = []
  let provinces = []
  let districts = []
  for (const [key, value] of Object.entries(data)) {
    departments.push([key, value.name, '', ''])
    for (const [keyProvince, valueProvince] of Object.entries(value.provinces)) {
      provinces.push([keyProvince, valueProvince.name, key, value.name])
      for (const [keyDistrict, valueDistrict] of Object.entries(valueProvince.districts)) {
        districts.push([keyDistrict, valueDistrict.name, keyProvince, valueProvince.name])
      }
    }
  }
  return {'departments': departments, 'provinces': provinces, 'districts': districts}
}

interface TableWrapperProps {
  departments: DepartmentList,
}

const TableWrapper: React.FC<TableWrapperProps> = ({departments}) => {
  const data = format(departments)
  if (!data.departments.length && !data.provinces.length && !data.districts.length) {
    return <div className={classes.Message}>no data</div>
  }

  return (
    <div className={classes.HomeTable}>
      <Table key={1} title={'departamento'} data={data.departments} />
      <Table key={2} title={'provincia'} data={data.provinces} />
      <Table key={3} title={'distrito'} data={data.districts} />
    </div>)
}

export default TableWrapper