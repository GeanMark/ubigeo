import React from 'react'
import classes from './styles.module.scss'

interface Table {
  title: string,
  data: string[][]
}

const Table: React.FC<Table> = ({title, data}) => {
  return (
    <div className={classes.Table}>
      <h2>{title}</h2>
      <div className={classes.Column}>
        <div className={`${classes.Row} ${classes.Header}`}>
          <div>Codigo</div>
          <div>Nombre</div>
          <div>Codigo Padre</div>
          <div>Descripción Padre</div>
        </div>

        {data.map((e, i) => {
          return <div key={i} className={classes.Row}>
            <div>{e[0]}</div>
            <div>{e[1]}</div>
            <div>{e[2]}</div>
            <div>{e[3]}</div>
          </div>
        })}
      </div>
    </div>
  )
}

export default Table