import React from 'react'
import classes from './styles.module.scss'
import Input from '../../base/input/input'

interface Header {
  onChange?: (file: File) => void
}

const Header: React.FC<Header> = ({onChange}) => (
  <div className={classes.Header}>
    <Input id="file" type={'file'} onChange={onChange} className={classes.Input} />
    <label htmlFor='file'><span>Choose a file ...</span></label>
  </div>
)

export default Header