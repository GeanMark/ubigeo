export interface District {
  name: string
}

export interface Province {
  name: string
  districts: {
    [code: string]: District
  }
}

export interface Department {
  name: string
  provinces: {
    [code: string]: Province
  }
}

export interface DepartmentList {
  [code: string]: Department
}

export interface ReduxState {
  departments: DepartmentList
}

export const initialState: ReduxState = {
  departments: {}
}