import React from 'react'
import Header from '../../../components/home/header'
import { merge, parse } from '../../../utils/parser'
import { DepartmentList } from '../../../store/state'


interface HeaderWrapperProps {
  populateDepartments: (departments: DepartmentList) => void,
}

class HeaderWrapper extends React.PureComponent<HeaderWrapperProps> {

  handleChange = (e: any) => {
    const file = e.target.files[0]
    const reader = new FileReader()
    if (file instanceof Blob) {
      reader.readAsText(file)
      reader.onload = () => {
        const sampleData = reader.result as string
        const rows = parse(sampleData)
        const departments = merge(rows)
        this.props.populateDepartments(departments)
      }
    }
  }

  render() {
    return (
      <Header onChange={this.handleChange} />
    )
  }
}

export default HeaderWrapper