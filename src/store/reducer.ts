import { initialState } from './state'
import { DepartmentsActionTypes } from './types'

interface AnyAction {
  type: string,

  [key: string]: any,
}

export function reducer(state = initialState, action: AnyAction) {
  switch (action.type) {
    case DepartmentsActionTypes.POPULATE_DATA:
      return {
        ...initialState,
        departments: action.departments,
      }
    default:
      return state
  }
}