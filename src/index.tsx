import React from 'react'
import ReactDOM from 'react-dom'
import './index.css'
import App from './App'
import * as serviceWorker from './serviceWorker'
import { createStore, compose } from 'redux'
import { Provider } from 'react-redux'
import { reducer } from './store/reducer'

// dev tools middleware
const reduxDevTools =
  (w => w.__REDUX_DEVTOOLS_EXTENSION__ && w.__REDUX_DEVTOOLS_EXTENSION__())(window as any)
const args = [
  ...(reduxDevTools ? [reduxDevTools] : []),
]

// create a redux store with our reducer above and middleware
const store = createStore(
  reducer,
  compose(...args),
)

ReactDOM.render(
  <Provider store={store}>
    <App />
  </Provider>
  , document.getElementById('root'))

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister()
